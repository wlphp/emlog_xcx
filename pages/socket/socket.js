// pages/socket/socket.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    wx.connectSocket({
      url: 'ws://127.0.0.1:8080',
      success: function () {
        console.log("connect socket success")
        wx.showToast({
          title: 'connect socket success',
          icon: 'success',
          duration: 2000
        })

      },
      fail: function () {
        console.log("connect socket fail")
        wx.showToast({
          title: 'connect socket fail',
          icon: 'error',
          duration: 2000
        })
      }
    })
    wx.onSocketOpen(function (res) {
      console.log(res)
    })
    wx.onSocketMessage(function (res) {
      console.log(res);
      wx.showModal({
        title: '提示',
        content: res.data,
        success(res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })


    })

  },

  //websocket
  websocket: function () {
    let msg = " msg from client_weixin"
    wx.sendSocketMessage({
      data: msg
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})